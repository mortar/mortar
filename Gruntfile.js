module.exports = function (grunt) {

	var config = {
		paths: {
			development: 	'./development/',
			production: 	'./production/'
		}
	}

    grunt.initConfig({
    	config: config,

        pkg: grunt.file.readJSON('package.json'),

		devserver: {
			options: {
				type: 	'http',
				port: 	'<%= pkg.http.port%>',
				base: 	config.paths.production,
				async: 	true
			}
		},

		'http-server': {
			'dev': {
				root: config.paths.production,
				port: '<%= pkg.http.port %>',
				host: '<%= pkg.http.host %>',
				showDir : true,
				autoIndex: true,
				defaultExt: "html",
				runInBackground: false
			}
		},

	    watch: {
	        less: {
	            files: ['<%= config.paths.development %>less/**'],
	            tasks: ['less:development']
	        },
	        js: {
	            files: ['<%= config.paths.development %>js/*.js'],
	            tasks: ['js:development']
	        },
	        html: {
	            files: [
	            	'<%= config.paths.development %>pages/*.html', 
	            	'<%= config.paths.development %>partials/*.html'
	            ],
	            tasks: ['html']
	        },
	        misc: {
	            files: [
	            	'<%= config.paths.development %>fonts/**', 
	            	'<%= config.paths.development %>json/**', 
	            	'<%= config.paths.development %>img/**'
	            ],
	            tasks: ['copy']
	        }
	    },

		copy: {
			fonts: {
				expand: 	true,
    			flatten: 	true,
    			filter: 	'isFile',
				src: 		'<%= config.paths.development %>fonts/**',
				dest: 		'<%= config.paths.production %>assets/fonts/'
			},
			img: {
				expand: 	true,
    			flatten: 	true,
    			filter: 	'isFile',
				src: 		'<%= config.paths.development %>img/**',
				dest: 		'<%= config.paths.production %>assets/img/'
			}
		},

	    uglify: {
	        options: {
	            banner: '/* ----------------------------------------------------------------------------' + '\n' +
	                    'Author: <%= pkg.author.name %> (<%= pkg.author.email %>)' + '\n' +
	                    'Created: <%= grunt.template.today("yyyy-mm-dd") %>' + '\n' +
	                    '---------------------------------------------------------------------------- */' + '\n\n' +
	                    'console.log = function() {};'
	        },
	        application: {
	        	files: {
	            	'<%= config.paths.production %>assets/js/example.js': [
	                    '<%= config.paths.development %>lib/jquery/jquery.js',
	                    '<%= config.paths.development %>lib/accordion/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/breakpoints/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/carousel/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/collapse/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/accordion/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/lightbox/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/slideshow/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/tabs/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>js/app.js'
	                ]
	            }
	        },
	        respond: {
	        	files: {
	            	'<%= config.paths.production %>assets/js/respond.js': [
	                    '<%= config.paths.development %>js/respond.js'
	                ]
	            }
	        }
	    },

	    concat: {
		    options: {
		      	stripBanners: true,
		      	banner: '/* ----------------------------------------------------------------------------' + '\n' +
	                    'Author: <%= pkg.author.name %> (<%= pkg.author.email %>)' + '\n' +
	                    'Created: <%= grunt.template.today("yyyy-mm-dd") %>' + '\n' +
	                    '---------------------------------------------------------------------------- */' + '\n'
		    },
		    application: {
		      	src: [
	                    '<%= config.paths.development %>lib/jquery/jquery.js',
	                    '<%= config.paths.development %>lib/accordion/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/breakpoints/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/carousel/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/collapse/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/accordion/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/lightbox/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/slideshow/production/assets/js/plugin.js',
	                    '<%= config.paths.development %>lib/tabs/production/assets/js/plugin.js',
	                	'<%= config.paths.development %>js/app.js'
		      	],
		      	dest: '<%= config.paths.production %>assets/js/example.js',
		    },
		    respond: {
		      	src: [
	                	'<%= config.paths.development %>js/respond.js'
		      	],
		      	dest: '<%= config.paths.production %>assets/js/respond.js',
		    }
		},

	    recess: {
	        development: {
	            options: {
	                compile: 	true,
	                compress: 	false
	            },
	            files: {
	                '<%= config.paths.production %>assets/css/example.css':  '<%= config.paths.development %>less/example.less'
	            }
	        },
	        production: {
	            options: {
	                compile: 	true,
	                compress: 	true
	            },
	            files: {
	                '<%= config.paths.production %>assets/css/example.min.css':  '<%= config.paths.development %>less/example.less'
	            }
	        }
	    },

	    includes: {
			files: {
				src: 		'./*.html',
				dest: 		config.paths.production,
				flatten: 	true,
				cwd: 		config.paths.development + 'pages/',
				options: {
					silent: true,
					banner: '<!--' + '\n' +
	                    'URL: <%= pkg.repository.url %>' + '\n' +
	                    'Author: <%= pkg.author.name %> (<%= pkg.author.email %>)' + '\n' +
	                    'Created: <%= grunt.template.today("yyyy-mm-dd") %>' + '\n' +
	                    '-->' + '\n'
				}
			}
		}

    });
	
	grunt.registerTask('default', function() {
		var msg =	'What would you like to run? \n' +	
				
		'\n grunt server \n' + 
		' - This will run a local development server.\n' +

		'\n grunt bower \n' + 
		' - This will install bower dependancies.\n' +

		'\n grunt html \n' +
		' - This will compile your html.\n' +

		'\n grunt misc \n' + 
		' - This will copy misc assets (fonts, images, etc) into the production folder.\n' +

		'\n grunt less (:development/:production) \n' + 
		' - This will compile your less to css. \n' +

		'\n grunt build \n' + 
		' - This will do everything above (except for run devserver). \n';

		grunt.log.write( msg );
	});

	grunt.registerTask('install', function() {
        var exec = require('child_process').exec;
        var cb = this.async();
        exec('bower install', {cwd: '.'}, function(err, stdout, stderr) {
            console.log(stdout);
            cb();
        });
    });

	grunt.registerTask('update', function() {
        var exec = require('child_process').exec;
        var cb = this.async();
        exec('bower update', {cwd: '.'}, function(err, stdout, stderr) {
            console.log(stdout);
            cb();
        });
    });

	grunt.registerTask('server', 			['http-server']);
	grunt.registerTask('bower', 			['install', 'update']);
	grunt.registerTask('html', 				['includes']);
	grunt.registerTask('js:development', 	['concat']);
	grunt.registerTask('js:production', 	['uglify']);
	grunt.registerTask('less', 				['recess']);
	grunt.registerTask('misc', 				['copy']);
	grunt.registerTask('build', 			['html', 'less', 'js:development', 'misc', 'watch']);

	grunt.loadNpmTasks('grunt-devserver');
	grunt.loadNpmTasks('grunt-http-server');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-recess');
    grunt.loadNpmTasks('grunt-includes');

};